<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\Btrip\ReturnPostController;
use App\Http\Controllers\Btrip\TripBookingController;
// Admin
use App\Http\Controllers\Admin\AdminAuthController;
use App\Http\Controllers\Admin\AdminReturnPostController;
// User
use App\Http\Controllers\UserReturnTripController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:sanctum'])->get('/user', function (Request $request) {
    return $request->user();
});

require __DIR__.'/auth.php';

// User Route
Route::controller(AuthController::class)->group(function () {
    Route::get('/logout', 'destroy');
    Route::post('/change-password', 'ChangePassword');
});
Route::controller(ProfileController::class)->group(function () {
    Route::post('/profile-update', 'ProfileUpdate');
});

Route::controller(ReturnPostController::class)->group(function () {
    Route::get('/btrip/return-post', 'ReturnPosts');
    Route::post('/btrip/return-post', 'ReturnPost');
    Route::post('/btrip/return-post/search', 'SearchPosts');
    Route::get('/btrip/return-post-details/{id}', 'ReturnPostDetails');
});

Route::controller(UserReturnTripController::class)->group(function () {
    Route::get('/user/trip-booked-data/{id}', 'UserTripBooked');
});



// ====================================================
// Test Route
Route::get('/test', [AuthController::class, 'TestFunction']);

// Admin Route
Route::controller(AdminAuthController::class)->group(function () {
    Route::get('/admin/user-data', 'AdminUser');
    Route::post('/admin/login', 'AdminLogin');
    Route::get('/admin/logout', 'AdminLogout');
});
Route::controller(AdminReturnPostController::class)->group(function () {
    Route::get('/btrip/return-post-list', 'ReturnPostsList');
    Route::get('/btrip/passenger-view-data/{id}', 'PassengeriewData');
    Route::get('/btrip/driver-view-data/{id}', 'DriveriewData');
    Route::get('/admin/btrip/return-post-data/{id}', 'AdminReturnPostData');
    Route::post('/admin/btrip/return-post-edit-data', 'AdminReturnPostEdit');

    Route::post('/admin/btrip/post-publish/{id}', 'AdminPostPublish');
    Route::post('/admin/btrip/post-unpublish/{id}', 'AdminPostUnpublish');
});
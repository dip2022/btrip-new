<?php

namespace App\Http\Controllers\Btrip;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ReturnPost;
use App\Models\User;
use Illuminate\Support\Carbon;

class ReturnPostController extends Controller
{
    public function ReturnPosts(){
        $posts = ReturnPost::with('userinfo')->latest()->get();
        return response()->json($posts);
    }
    public function ReturnPost(Request $request)
    {
        $request->validate([
            'post_data' => ['required', 'string'],
            'post_bg' => ['required', 'string'],
            'pickup_point' => ['required', 'string'],
            'dropping_point' => ['required', 'string'],
            'phone_number' => ['required', 'numeric', 'digits_between:1,14'],
            'car_type' => ['required', 'string'],
            'car_number_plate' => ['required'],
            'seat_number' => ['required'],
            'post_amount' => ['required'],
            'date_of_journey' => ['required'],
        ]);
        
        $post = ReturnPost::create([
            'user_post_id' => $request->user_post_id,
            'post_data' => $request->post_data,
            'post_bg' => $request->post_bg,
            'pickup_point' => $request->pickup_point,
            'dropping_point' => $request->dropping_point,
            'phone_number' => $request->phone_number,
            'car_type' => $request->car_type,
            'car_number_plate' => $request->car_number_plate,
            'seat_number' => $request->seat_number,
            'post_amount' => $request->post_amount,
            'date_of_journey' => $request->date_of_journey,
            'created_at' => Carbon::now(),
        ]);

        return response()->json(['status'=>'Return Post Create Successfully']);
    }
    public function SearchPosts(Request $request){
        $posts = ReturnPost::where('pickup_point',$request->pickup_point)
                ->where('dropping_point',$request->dropping_point)
                ->where('car_type',$request->car_type)
                ->with('userinfo')->latest()->get()->limit(8);
        return response()->json($posts);
    }

    public function ReturnPostDetails($id){
        $post = ReturnPost::find($id)->with('userinfo')->first();
        return response()->json($post);
    }

}

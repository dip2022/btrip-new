<?php

namespace App\Http\Controllers\Btrip;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ReturnPost;
use App\Models\TripBooking;
use Illuminate\Support\Carbon;

class TripBookingController extends Controller
{
    public function TripBookingPay(Request $request){
        $randomNumber = random_int(100000, 999999);
        $data = TripBooking::insert([
            'post_id' => $request->post_id,
            'user_post_id' => $request->user_post_id,
            'user_booking_id' => $request->user_booking_id,
            
            'payment_by' => $request->payment_by,
            'phone_number' => $request->phone_number,
            'transition_id' => $request->transition_id,
            'created_at' => Carbon::now(),
            
        ]);
        if($data){
            ReturnPost::where('id','=',$request->post_id)->update([
                'booking_otp'=>$randomNumber,
                'user_booking_id'=>$request->user_booking_id,
                'user_booking_phone' => $request->phone_number,
                'booking_status' => 1,
            ]);
            return response()->json(['status'=>'Trip Booking Successfully.']);
        }else{
            return response()->json(['status'=>'Something Wrong!']);
        }
    }
}

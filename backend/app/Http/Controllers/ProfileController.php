<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Carbon;
use Image;

class ProfileController extends Controller
{
    public function ProfileUpdate(Request $request){
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'phone' => ['required', 'numeric', 'digits_between:1,14'],
            'address' => ['required', 'string'],
            'thana' => ['required'],
            'district' => ['required'],
            'driver_passenger' => ['required'],
            // 'image' => ['required'],
        ]);

        if($request->file('image')){
            $image = $request->file('image');
            $name_gen = hexdec(uniqid()).$image->getClientOriginalName();
            Image::make($image)->resize(200, 200)->save('uploads/users/'.$name_gen);
            $save_url = env('APP_URL').'/uploads/users/'.$name_gen;
        }else{
            $save_url = NULL;
        }
        
        $id = Auth::user()->id;
        if(Auth::user()->email == $request->email){
            $update = User::where('id',$id)->update([
                'name' => $request->name,
                // 'email' => $request->email,
                'phone' => $request->phone,
                'address' => $request->address,
                'thana' => $request->thana,
                'district' => $request->district,
                'driver_passenger' => $request->driver_passenger,
                'driving_license' => $request->driving_license,
                'car_plate_num' => $request->car_plate_num,
                'image' => $save_url,
                'updated_at' => Carbon::now(),
            ]);
            if($update){
                return response()->json(['status'=>'Profile Update Successfully']);
            }else{
                return response()->json(['status'=>'Profile Not Updated!']);
            }
        }else{
            return response()->json(['status'=>'Email Not Match!']);
        }
    }
}

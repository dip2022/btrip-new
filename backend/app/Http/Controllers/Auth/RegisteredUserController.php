<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\Support\Carbon;
use Image;

class RegisteredUserController extends Controller
{
    /**
     * Handle an incoming registration request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): Response
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:'.User::class],
            'phone' => ['required', 'numeric', 'digits_between:1,14'],
            'password' => ['required'],
            // 'password' => ['required', 'confirmed', Rules\Password::defaults()],
            'address' => ['required', 'string'],
            'thana' => ['required'],
            'district' => ['required'],
            'driver_passenger' => ['required'],
            // 'image' => ['required'],
        ]);

        if($request->file('image')){
            $image = $request->file('image');
            $name_gen = hexdec(uniqid()).$image->getClientOriginalName();
            Image::make($image)->resize(200, 200)->save('uploads/users/'.$name_gen);
            $save_url = env('APP_URL').'/uploads/users/'.$name_gen;
        }else{
            $save_url = '';
        }
        
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => Hash::make($request->password),
            'address' => $request->address,
            'thana' => $request->thana,
            'district' => $request->district,
            'driver_passenger' => $request->driver_passenger,
            'driving_license' => $request->driving_license,
            'car_plate_num' => $request->car_plate_num,
            'image' => $save_url,
            'created_at' => Carbon::now(),
        ]);

        event(new Registered($user));

        Auth::login($user);

        return response()->noContent();
    }
}

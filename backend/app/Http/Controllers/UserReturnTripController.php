<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TripBooking;

class UserReturnTripController extends Controller
{
    public function UserTripBooked($id){
        $posts = TripBooking::where('user_booking_id',$id)->with('postinfo','postuser','bookeduser')->get();
        return response()->json($posts);
    }
}

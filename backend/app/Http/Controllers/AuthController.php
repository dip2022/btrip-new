<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;

class AuthController extends Controller
{
    public function ChangePassword(Request $request){
        $validateData = $request->validate([
            'old_password' => 'required',
            'password' => 'required',
            'confirm_password' => 'required|same:password',
        ]);
        
        $hashedPassword = Auth::user()->password;
        if(Hash::check($request->old_password, $hashedPassword)){
            $user = User::find(Auth::user()->id);
            $user->password = bcrypt($request->password);
            $user->save();
            return response()->json(['status'=>'Password Update Successfully']);
        }else{
            return response()->json(['status'=>'Old Password is not match!']);
        }
    }

    public function destroy(Request $request): Response
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return response()->noContent();
    }

    


    public function TestFunction(){
        $host = $_SERVER['HTTP_HOST'];
        $host1 = $_SERVER['SERVER_NAME'];
        // $imgName = explode('/',$img_path)[2];
        // $img_location = "http://".$host."/storage/user/".$imgName;
        // return env('APP_PROTOCOL');
        return $host;
    }
}

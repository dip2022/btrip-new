<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ReturnPost;
use App\Models\User;
use Illuminate\Support\Carbon;

class AdminReturnPostController extends Controller
{
    // Admin Function
    public function ReturnPostsList(){
        $posts = ReturnPost::with('userinfo')->latest()->get();
        return response()->json($posts);
    }
    public function PassengeriewData($id){
        $viewData = User::where('id',$id)->first();
        return response()->json($viewData);
    }
    public function DriveriewData($id){
        $viewData = User::where('id',$id)->first();
        return response()->json($viewData);
    }
    public function AdminReturnPostData($id){
        $post = ReturnPost::where('id',$id)->with('userinfo')->first();
        return response()->json($post);
    }
    public function AdminReturnPostEdit(Request $request){
        $post = ReturnPost::where('id',$request->post_id)->update([
            'post_data' => $request->post_data,
            'post_bg' => $request->post_bg,
            'pickup_point' => $request->pickup_point,
            'dropping_point' => $request->dropping_point,
            'phone_number' => $request->phone_number,
            'car_type' => $request->car_type,
            'car_number_plate' => $request->car_number_plate,
            'seat_number' => $request->seat_number,
            'post_amount' => $request->post_amount,
            'date_of_journey' => $request->date_of_journey,
        ]);
        if($post){
            return response()->json(['status'=>'Return Post Update Successfully.']);
        }else{
            return response()->json(['status'=>'Something Wrong!']);
        }
    }

    public function AdminPostPublish($id){
        $post = ReturnPost::where('id',$id)->update([
            'status' => 1,
        ]);
        if($post){
            return response()->json(['status'=>'Return Post Publish Successfully.']);
        }else{
            return response()->json(['status'=>'Something Worng!']);
        }
    }
    public function AdminPostUnpublish($id){
        $post = ReturnPost::where('id',$id)->update([
            'status' => 0,
        ]);
        if($post){
            return response()->json(['status'=>'Return Post Publish Successfully.']);
        }else{
            return response()->json(['status'=>'Return Post not Unpublish!']);
        }
    }
}

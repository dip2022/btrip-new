<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AdminAuthController extends Controller
{
    public function AdminUser()
    {
        $admin_email = Session::get('admin_email');
        if ($admin_email) {
            $data = User::where('email',$admin_email)->where('status','active')->first();
            return $data;
        }else{
            return response()->json(['status'=>'Something Worng!']);
        }
    }
    public function AdminLogin(Request $request){
        $this->validate($request, [
            'email' => ['required', 'string', 'email'],
            'password' => ['required', 'string'],
        ]);
        
        $data = User::where('email',$request->email)->where('status','active')->first();
        if (User::where('email',$request->email)->where(Hash::check($request->password, $data->password))->where('role','admin')) {
            Session::put('admin_email',$request->email);
            return $data;
        }else{
            return response()->json(['status'=>'Email and Password are not match!']);
        }
    }

    public function AdminLogout()
    {
        Session::flash('admin_email');
        return;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TripBooking extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function userinfo(){
        return $this->belongsTo(User::class,'user_post_id','id')->select('id','name','image','driver_passenger');
    }

    public function postinfo(){
        return $this->belongsTo(ReturnPost::class,'post_id','id')->select('id','pickup_point','dropping_point','phone_number','car_type','car_number_plate','seat_number','post_amount','date_of_journey','booking_otp');
    }
    public function postuser(){
        return $this->belongsTo(User::class,'user_post_id','id')->select('id','name','image','driver_passenger','address');
    }
    public function bookeduser(){
        return $this->belongsTo(User::class,'user_booking_id','id')->select('id','name','image','driver_passenger');
    }
    
}

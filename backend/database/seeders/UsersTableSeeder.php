<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            [
                // Admin
                'name'=>'Admin',
                'username'=>'admin',
                'email'=>'admin@gmail.com',
                'password'=>Hash::make('password'),
                'return_user'=>'driver',
                'role'=>'admin',
                'status'=>'active',
            ],
            [
                // Vendor
                'name'=>'Dip Vendor',
                'username'=>'vendor',
                'email'=>'vendor@gmail.com',
                'password'=>Hash::make('password'),
                'return_user'=>'driver',
                'role'=>'vendor',
                'status'=>'active',
            ],
            [
                // User
                'name'=>'Dipankar',
                'username'=>'user',
                'email'=>'user@gmail.com',
                'password'=>Hash::make('password'),
                'return_user'=>'passenger',
                'role'=>'user',
                'status'=>'active',
            ],
        ]);
    }
}

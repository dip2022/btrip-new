<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('return_posts', function (Blueprint $table) {
            $table->id();
            $table->integer('user_post_id');
            $table->text('post_data');
            $table->string('post_bg');
            $table->string('pickup_point');
            $table->string('dropping_point');
            $table->string('phone_number')->nullable();
            $table->string('car_type')->nullable();
            $table->string('car_number_plate')->nullable();
            $table->string('seat_number')->nullable();
            $table->string('post_amount')->nullable();
            $table->string('date_of_journey')->nullable();
            $table->string('booking_otp')->nullable();
            $table->integer('user_booking_id')->nullable();
            $table->string('user_booking_phone')->nullable();
            $table->integer('admin_action')->default(0);
            $table->integer('driver_action')->default(0);
            $table->integer('booking_status')->default(0);
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('return_posts');
    }
};

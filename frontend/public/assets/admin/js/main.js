

$('body').on('click','#sidebar_toggle',function(){
    let mainSidebar = $('#main_page');
    mainSidebar.toggleClass('slide_sidebar');
})

$('body').on('click','.sidebar_menu li',function(){
    $(this).addClass('active').siblings().removeClass('active');
})

$('body').on('click','.upload_click',function(){
    $('.upload-images input').trigger('click');
})

// Gallery Item click Modal Show
$('body').on('click','.gallery .gallery_items li',function(){
    $(this).addClass('active').siblings().removeClass('active');
    // $('#galleryModal').modal('show');
})

// Gallary Item Active Remove
$('body').on('click','.select_img_items li',function(event){
    $(this).addClass('active').siblings().removeClass('active');
})

// Slider Img Select
$('body').on('click','.upload_img_file img',function(){
    $('#selectImgModal').modal('show');
})

$(window).on('load', function(){
    let bodyHeight = $('.overview_wrapper').innerHeight();
    if(bodyHeight < 500){
        $('#main_page').addClass('f_fixed')
    }
    let windowWidth = $(this).outerWidth();
    if(windowWidth < 768){
        $('#main_page').addClass('slide_sidebar')
    }
})

// DataTables
// $('#myTable').DataTable();



// ===============================
// Post Add/Edit Page
// Enter Key
$('.post .left .top textarea').keyup(function (e) {
    let textarea_rows = $(this).attr('rows');
    if (e.keyCode == 13) {
        textarea_rows++;
        $(this).attr('rows',textarea_rows);
    }
});
// BackSpace Key
$('.post .left .top textarea').keyup(function (e) {
    if (e.keyCode == 8) {
        // Same Time All Data Delete
        let txt_val_chk = $(this).val();
        if(txt_val_chk == ""){
            $(this).attr('rows',1);
        }

        // Single Line Delete 
        let text = $(this).val();
        let lines = text.split('\n');
        for (let index in lines) {
            var textarea_lines_number = lines.length
        }
        $(this).attr('rows',textarea_lines_number);
    }
});

// Sidebar Add category
$('.post .widget span.cat_add').click(function(){
    $(this).closest('.widget').find('.cat_add_div').toggleClass('d-none');
})
// Sidebar Text Size Select
$('.txt_size_list li').click(function(){
    $(this).addClass('active').siblings().removeClass('active');
    let title = $(this).attr('title');
    $('.block .text .txt_size').find('.slt_txt_size').html('');
    $('.block .text .txt_size').find('.slt_txt_size').html(title);
})
$('.block .txt_size .txt_size_add').click(function(){
    if($(this).hasClass('active')){
        $(this).removeClass('active');
        $(this).closest('.txt_size').find('.slt_txt_size').html('');
        $(this).closest('.txt_size').find('.slt_txt_size').html('Default');
        $('.block .text').find('.txt_size_list').removeClass('d-none');
        $('.block .text').find('.ctm_txt_type').addClass('d-none');
    }else{
        $(this).addClass('active');
        $(this).closest('.txt_size').find('.slt_txt_size').html('');
        $(this).closest('.txt_size').find('.slt_txt_size').html('Custom');
        $('.block .text').find('.ctm_txt_type').removeClass('d-none');
        $('.block .text').find('.txt_size_list').addClass('d-none');
    }
})
// Sidebar Text Color Select
$('.color_picker .list li').click(function(){
    let color = $(this).attr('value');
    $('.color_picker input').attr('value',color);
})







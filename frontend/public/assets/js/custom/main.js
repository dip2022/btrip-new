$(document).ready(function(){
    // Mobile Slide Active Class
    $('body').on('click','.nav_slide .nav_toggle .nav_toggle_btn', function(){
        nav_slide_open();
    })
    $('body').on('click','.nav_slide .nav_items .slide_close i', function(){
        nav_slide_close();
    })
    $('body').on('click','#site_overlay', function(){
        nav_slide_close();
    })
    function nav_slide_open(){
        $('nav.mobile_nav .nav_slide .nav_items').addClass('slide_active');
        $('#site_overlay').addClass('active');
    }
    function nav_slide_close(){
        $('nav.mobile_nav .nav_slide .nav_items').removeClass('slide_active');
        $('#site_overlay').removeClass('active');
    }


    // Service Icon Active
    $('body').on('click','.left_sidebar .services li', function(){
        $(this).addClass('active').siblings().removeClass('active');
    })

    // Driver Passenger Active
    $('body').on('click','.left_sidebar .driver_passenger .btn', function(){
        $(this).addClass('active').siblings().removeClass('active');
    })

    // Login Skip Action
    $('body').on('click','section.top_banner .login .skip', function(){
        localStorage.setItem('UserLoginSkip','Dip');
        UserLoginSkip();
    })
    // UserLoginSkip();
    function UserLoginSkip(){
        if(localStorage.getItem('UserLoginSkip') === 'Dip'){
            $('section.top_banner').css({"display": "none"});
            $('section.services').css({"display": "block"});
        }
    }

    // Services Click Action
    $('body').on('click','section.services .single_service', function(){
        let services_name = $(this).find('img').attr('alt');
        localStorage.setItem('bTripServicesName',services_name);
        bTripServices();
    })
    function bTripServices(){
        if(localStorage.getItem('bTripServicesName') === 'Return Trip'){
            $('section.services').css({"display": "none"});
            $('.main_wrap .wrapper .middle').css({"display": "block"});
        }
    }

    // Register Driver Passenger Select
    // $('body').on('click','.register_form .select_option input', function(){
    //     let checked = $(this).val();
    //     if(checked !== 'Driver'){
    //         $('.register_form .driver_field').css({"display": "none"});
    //     }else{
    //         $('.register_form .driver_field').css({"display": "block"});
    //     }
    // })

    // Create Post Click
    // $('body').on('click','.destop_create_post, .search_create .create_post button', function(){
    //     $('#CreatePostModal').modal('show')
    // })
    // $('body').on('click','#CreatePostModal .modal-footer button', function(){
    //     window.location.href = 'index.html';
    // })

    // Payment Pay Btn
    // $('body').on('click','.booking_pay .footer .payment_btn', function(){
    //     $('#PaymentModal').modal('show')
    // })
    // $('body').on('click','#PaymentModal .modal-footer button', function(){
    //     window.location.href = 'index.html';
    // })



    // Header Scroll Way
    
    // if(($('header.header').length)){
    //     let header = $('header.header');
    //     let headerTop = $('header.header').offset().top;
    //     let headerHeight = $('header.header').outerHeight();

    //     let heightCount = headerTop + $('.top_banner').height();

    //     console.log(headerHeight);
    //     console.log($(window).outerHeight());

    //     $(window).scroll(function(){
    //         // let windowTop = $(window).scrollTop();
    //         // // if((headerTop < windowTop)){
    //         // //     header.css({
    //         // //         'position':'fixed','top':0,'background-color':'#fff','width':'100%','z-index':999,
    //         // //     })
    //         // // }
    //         // // else{
    //         // //     header.css({
    //         // //         'position':'relative','top':'auto','background-color':'transparent',
    //         // //     })
    //         // // }

    //     })
    // }
})
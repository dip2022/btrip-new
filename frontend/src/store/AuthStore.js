import axios from "axios";
import router from "@/router/index.js";

const { reactive,  computed, readonly } = require("vue");

const state = reactive({
    authUser:null,
    _token:null,
    name:'',
    phone:'01741571104',
    email:'user@gmail.com',
    password:'password',
    confirm_password:'password',
    old_password:'password',
    address:'',
    district:'Dhaka',
    thana:'Dhaka',
    driver_passenger:'passenger',
    driving_license:'',
    car_plate_num:'',
    image:'',
    image_url:'',

    show_hide_profile_edit:false,
    DriverPassenger:false,

    success_msg:'',
    error_msg:'',
})

const getters = {
    authUser : () => {
        return computed(() => state.authUser);
    },
    _token : () => {
        return computed(() => state._token);
    },
    name : () => {
        return computed({
            get(){ return state.name },
            set(value){ return state.name = value },
        })
    },
    phone : () => {
        return computed({
            get(){ return state.phone },
            set(value){ return state.phone = value },
        })
    },
    email : () => {
        return computed({
            get(){ return state.email },
            set(value){ return state.email = value },
        })
    },
    password : () => {
        return computed({
            get(){ return state.password },
            set(value){ return state.password = value },
        })
    },
    confirm_password : () => {
        return computed({
            get(){ return state.confirm_password },
            set(value){ return state.confirm_password = value },
        })
    },
    old_password : () => {
        return computed({
            get(){ return state.old_password },
            set(value){ return state.old_password = value },
        })
    },
    address : () => {
        return computed({
            get(){ return state.address },
            set(value){ return state.address = value },
        })
    },
    district : () => {
        return computed({
            get(){ return state.district },
            set(value){ return state.district = value },
        })
    },
    thana : () => {
        return computed({
            get(){ return state.thana },
            set(value){ return state.thana = value },
        })
    },
    driver_passenger : () => {
        return computed({
            get(){ return state.driver_passenger },
            set(value){ return state.driver_passenger = value },
        })
    },
    driving_license : () => {
        return computed({
            get(){ return state.driving_license },
            set(value){ return state.driving_license = value },
        })
    },
    car_plate_num : () => {
        return computed({
            get(){ return state.car_plate_num },
            set(value){ return state.car_plate_num = value },
        })
    },
    image : () => {
        return computed({
            get(){ return state.image },
            set(value){ return state.image = value },
        })
    },
    image_url : () => {
        return computed({
            get(){ return state.image_url },
            set(value){ return state.image_url = value },
        })
    },
    
    success_msg : () => {
        return computed(() => state.success_msg);
    },
    error_msg : () => {
        return computed(() => state.error_msg);
    },

    
// Profile
    DriverPassenger : () => {
        return computed({
            get(){ return state.DriverPassenger },
            set(value){ return state.DriverPassenger = value },
        })
    },
    show_hide_profile_edit : () => {
        return computed({
            get(){ return state.show_hide_profile_edit },
            set(value){ return state.show_hide_profile_edit = value },
        })
    },
}

const mutations = {
    async getToken(){
        await axios.get('/sanctum/csrf-cookie');
    },
    async getUser(){
        mutations.getToken();
        const data = await axios.get('/api/user');
        state.authUser = data.data;
        localStorage.setItem('authId',data.data.id);
        localStorage.setItem('_token',document.cookie.split('=')[1]);
    },
    async SuccessMsg(data){
        state.error_msg = '';
        state.success_msg = data.data.status;
    },
    async ErrorMsg(error){
        state.success_msg = '';
        state.error_msg = error.message;
    },
}

const actions = {
    async RegPicBtn(){
        document.querySelector('#reg_img_select_image').click();
    },
    async RegPicChange(event){
        state.image = event.target.files[0];
        state.image_url = URL.createObjectURL(event.target.files[0]);
    },
    async DriverPassengerBtn(data){
        if(data == 'driver'){
            state.DriverPassenger = true;
        }else{
            state.DriverPassenger = false;
        }
    },
    async handleRegister(){
        try{
            await mutations.getToken();

            let formData = new FormData();
            formData.append('name',state.name);
            formData.append('email',state.email);
            formData.append('phone',state.phone);
            formData.append('password',state.password);
            formData.append('address',state.address);
            formData.append('thana',state.thana);
            formData.append('district',state.district);
            formData.append('driver_passenger',state.driver_passenger);
            formData.append('driving_license',state.driving_license);
            formData.append('car_plate_num',state.car_plate_num);
            formData.append('image',state.image);

            await axios.post('/api/register',formData);
            await mutations.getUser();
            router.push('/');
        }catch(error){
            if(error.response.status == '422'){
                mutations.ErrorMsg(error.response.data);
            }
        }
    },
    async handleLogin(){
        await mutations.getToken();
        try{
            await axios.post('/api/login',{
                email:state.email,
                password:state.password,
            });
            await mutations.getUser();
            router.push('/');
        
        }catch(error){
            if(error.response.status == '422'){
                mutations.ErrorMsg(error.response.data);
            }
        }
        
    },
    async Logout(){
        await axios.get('/api/logout');
        state.authUser = null;
        localStorage.removeItem('authId');
        localStorage.removeItem('_token');
        router.push('/');
    },
    async handleForgetPassword(){
        await mutations.getToken();
        try{
            const data = await axios.post('/api/forgot-password',{
                email:state.email
            });
            mutations.SuccessMsg(data);
        }catch(error){
            if(error.response.status == '422'){
                mutations.ErrorMsg(error.response.data);
            }
        }
    },
    async handleResetPassword(){
        await mutations.getToken();
        try{
            await axios.post('/api/reset-password',{
                email:state.email,
                password:state.password,
                confirm_password:state.confirm_password,
                token:state._token
            });
            router.push('/login');
        }catch(error){
            if(error.response.status == '422'){
                mutations.ErrorMsg(error.response.data);
            }
        }
    },
    async handleChangePassword(){
        await mutations.getToken();
        try{
            const data = await axios.post('/api/change-password',{
                old_password:state.old_password,
                password:state.password,
                confirm_password:state.confirm_password,
            });
            mutations.SuccessMsg(data);
        }catch(error){
            if(error.response.status == '422'){
                mutations.ErrorMsg(error.response.data);
            }
        }
    },
    async handleProfileUpdate(){
        try{
            await mutations.getToken();

            let formData = new FormData();
            formData.append('name',state.name);
            formData.append('email',state.email);
            formData.append('phone',state.phone);
            formData.append('address',state.address);
            formData.append('thana',state.thana);
            formData.append('district',state.district);
            formData.append('driver_passenger',state.driver_passenger);
            formData.append('driving_license',state.driving_license);
            formData.append('car_plate_num',state.car_plate_num);
            formData.append('image',state.image);

            await axios.post('/api/profile-update',formData);
            await mutations.getUser();
            state.show_hide_profile_edit = false;
        }catch(error){
            if(error.response.status == '422'){
                mutations.ErrorMsg(error.response.data);
            }
        }
    },


    // Profile
    async ProfileEditBtn(){
        state.show_hide_profile_edit = true;
        state.name = state.authUser.name;
        state.phone = state.authUser.phone;
        state.email = state.authUser.email;
        state.address = state.authUser.address;
        state.district = state.authUser.district;
        state.thana = state.authUser.thana;
        state.driver_passenger = state.authUser.driver_passenger;
        state.driving_license = state.authUser.driving_license;
        state.car_plate_num = state.authUser.car_plate_num;
    }
}

export default () => {
    return {
        state:readonly(state),
        ...getters,
        ...mutations,
        ...actions,
    }
}
import axios from "axios";
import router from "@/router/index.js";

const { reactive,  computed, readonly } = require("vue");

const state = reactive({
    AdminAuthUser:null,
    email:'user@gmail.com',
    password:'password',

    success_msg:'',
    error_msg:'',
})

const getters = {
    AdminAuthUser : () => {
        return computed(() => state.AdminAuthUser);
    },
    email : () => {
        return computed({
            get(){ return state.email },
            set(value){ return state.email = value },
        })
    },
    password : () => {
        return computed({
            get(){ return state.password },
            set(value){ return state.password = value },
        })
    },
    
    success_msg : () => {
        return computed(() => state.success_msg);
    },
    error_msg : () => {
        return computed(() => state.error_msg);
    },

}

const mutations = {
    async getToken(){
        await axios.get('/sanctum/csrf-cookie');
    },
    async getAdminUser(){
        mutations.getToken();
        const data = await axios.get('/api/admin/user-data');
        state.AdminAuthUser = data.data;
    },
    async SuccessMsg(data){
        state.error_msg = '';
        state.success_msg = data.data.status;
    },
    async ErrorMsg(error){
        state.success_msg = '';
        state.error_msg = error.message;
    },
}

const actions = {
    async handleAdminLogin(){
        await mutations.getToken();
        try{
            const data = await axios.post('/api/admin/login',{
                email:state.email,
                password:state.password,
            });

            state.AdminAuthUser = data.data;
            localStorage.setItem('AdminAuthId',data.data.id);
            localStorage.setItem('_admin_token',document.cookie.split('=')[1]);

            router.push('/admin');
        
        }catch(error){
            if(error.response.status == '422'){
                mutations.ErrorMsg(error.response.data);
            }
        }
    },
    async AdminLogout(){
        await axios.get('/api/admin/logout');
        state.AdminAuthUser = null;
        localStorage.removeItem('AdminAuthId');
        localStorage.removeItem('_admin_token');
        router.push('/admin/login');
    },
}

export default () => {
    return {
        state:readonly(state),
        ...getters,
        ...mutations,
        ...actions,
    }
}
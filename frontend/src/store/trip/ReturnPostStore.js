import axios from "axios";
// import router from "@/router/index.js";
import AuthStore from "@/store/AuthStore.js";

const { reactive,  computed, readonly } = require("vue");
const { authUser, getToken } = AuthStore();

const state = reactive({
    all_return_post_data:null,
    TripPosts:null,
    create_post_modal:false,
    PostBg:[
        {id:1,bg_color:'background-color: rgb(255, 255, 255); color: rgb(68, 68, 68);'},
        {id:2,bg_color:'background: linear-gradient(60deg, rgb(188, 104, 159), rgb(0, 132, 165)); color: rgb(255, 255, 255);'},
        {id:3,bg_color:'background: linear-gradient(90deg, rgb(134, 209, 105), rgb(65, 183, 130)); color: rgb(255, 255, 255);'},
        {id:4,bg_color:'background: linear-gradient(rgb(230, 100, 101), rgb(145, 152, 229)); color: rgb(255, 255, 255);'},
        {id:5,bg_color:'background: linear-gradient(135deg, rgb(143, 199, 173), rgb(72, 229, 169)); color: rgb(255, 255, 255);'},
        {id:6,bg_color:'background: linear-gradient(to right, rgb(236, 111, 102), rgb(243, 161, 131)); color: rgb(255, 255, 255);'},
        {id:7,bg_color:'background: linear-gradient(to right, rgb(116, 116, 191), rgb(52, 138, 199)); color: rgb(255, 255, 255);'},
        {id:8,bg_color:'background: linear-gradient(to right, rgb(218, 226, 248), rgb(214, 164, 164)); color: rgb(68, 68, 68);'},
        {id:9,bg_color:'background: linear-gradient(to right, rgb(35, 37, 38), rgb(65, 67, 69)); color: rgb(255, 255, 255);'},
        {id:9,bg_color:'background: linear-gradient(to right top, rgb(86, 113, 153), rgb(54, 132, 159), rgb(57, 147, 147), rgb(99, 158, 126), rgb(148, 164, 110)); color: rgb(255, 255, 255);'},
        {id:9,bg_color:'background: linear-gradient(to right top, rgb(45, 97, 174), rgb(0, 117, 177), rgb(0, 132, 165), rgb(0, 143, 144), rgb(60, 151, 123)); color: rgb(255, 255, 255);'},
        {id:9,bg_color:'background: linear-gradient(to right top, rgb(45, 146, 174), rgb(14, 160, 178), rgb(0, 173, 176), rgb(35, 185, 169), rgb(72, 196, 158)); color: rgb(255, 255, 255);'},
        {id:9,bg_color:'background: linear-gradient(to right top, rgb(87, 159, 105), rgb(144, 145, 69), rgb(186, 124, 72), rgb(205, 105, 109), rgb(188, 104, 159)); color: rgb(255, 255, 255);'},
        {id:9,bg_color:'background: linear-gradient(to right top, rgb(131, 201, 149), rgb(145, 204, 146), rgb(159, 206, 143), rgb(172, 209, 141), rgb(186, 211, 140)); color: rgb(255, 255, 255);'},
        {id:9,bg_color:'background: linear-gradient(to right top, rgb(131, 201, 164), rgb(132, 204, 172), rgb(134, 206, 180), rgb(136, 209, 188), rgb(140, 211, 195)); color: rgb(255, 255, 255);'},
        {id:9,bg_color:'background: linear-gradient(to right top, rgb(170, 131, 201), rgb(231, 132, 177), rgb(255, 150, 142), rgb(253, 183, 116), rgb(220, 220, 123)); color: rgb(255, 255, 255);'},
        {id:9,bg_color:'background: linear-gradient(to right top, rgb(76, 190, 175), rgb(86, 183, 144), rgb(106, 175, 111), rgb(128, 164, 80), rgb(151, 151, 52)); color: rgb(255, 255, 255);'},
    ],
    post_bg_active:0,
    post_data:'',
    post_bg:'',
    pickup_point:'',
    dropping_point:'',
    phone_number:'',
    car_type:'',
    car_number_plate:'',
    seat_number:'',
    post_amount:'',
    date_of_journey:'',
})

const getters = {
    all_return_post_data : () => {
        return computed(() => state.all_return_post_data);
    },
    TripPosts : () => {
        return computed(() => state.TripPosts);
    },
    create_post_modal : () => {
        return computed({
            get(){ return state.create_post_modal },
            set(value){ return state.create_post_modal = value },
        })
    },
    PostBg : () => {
        return computed(() => state.PostBg);
    },
    post_bg_active : () => {
        return computed({
            get(){ return state.post_bg_active },
            set(value){ return state.post_bg_active = value },
        })
    },
    post_data : () => {
        return computed({
            get(){ return state.post_data },
            set(value){ return state.post_data = value },
        })
    },
    post_bg : () => {
        return computed({
            get(){ return state.post_bg },
            set(value){ return state.post_bg = value },
        })
    },
    pickup_point : () => {
        return computed({
            get(){ return state.pickup_point },
            set(value){ return state.pickup_point = value },
        })
    },
    dropping_point : () => {
        return computed({
            get(){ return state.dropping_point },
            set(value){ return state.dropping_point = value },
        })
    },
    phone_number : () => {
        return computed({
            get(){ return state.phone_number },
            set(value){ return state.phone_number = value },
        })
    },
    car_type : () => {
        return computed({
            get(){ return state.car_type },
            set(value){ return state.car_type = value },
        })
    },
    car_number_plate : () => {
        return computed({
            get(){ return state.car_number_plate },
            set(value){ return state.car_number_plate = value },
        })
    },
    seat_number : () => {
        return computed({
            get(){ return state.seat_number },
            set(value){ return state.seat_number = value },
        })
    },
    post_amount : () => {
        return computed({
            get(){ return state.post_amount },
            set(value){ return state.post_amount = value },
        })
    },
    date_of_journey : () => {
        return computed({
            get(){ return state.date_of_journey },
            set(value){ return state.date_of_journey = value },
        })
    },
}

const mutations = {
    async ReturnPosts(){
        await getToken();
        const data = await axios.get('/api/btrip/return-post');
        state.all_return_post_data = data.data;
    },
}

const actions = {
    async handleCreatePost(){
        try{
            await getToken();

            let formData = new FormData();
            formData.append('user_post_id',authUser().value.id);
            formData.append('post_data',state.post_data);
            formData.append('post_bg',state.post_bg);
            formData.append('pickup_point',state.pickup_point);
            formData.append('dropping_point',state.dropping_point);
            formData.append('phone_number',state.phone_number);
            formData.append('car_type',state.car_type);
            formData.append('car_number_plate',state.car_number_plate);
            formData.append('seat_number',state.seat_number);
            formData.append('post_amount',state.post_amount);
            formData.append('date_of_journey',state.date_of_journey);

            await axios.post('/api/btrip/return-post',formData);
            await mutations.ReturnPosts();
            state.create_post_modal = false;
        }catch(error){
            if(error.response.status == '422'){
                mutations.ErrorMsg(error.response.data);
            }
        }
    },
}

export default () => {
    return {
        state:readonly(state),
        ...getters,
        ...mutations,
        ...actions,
    }
}
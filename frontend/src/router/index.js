import { createWebHistory, createRouter } from "vue-router";


const routes = [
  // {
  //   path: "/",
  //   name: "home-page",
  //   component: HomePage,
  // },
];

const router = new createRouter({
  history:createWebHistory(),
  routes
});

// router.beforeEach((to, from, next) => {
//     next();
// })
router.beforeEach((to, from, next) => {
  if(to.meta.AuthUser){
      if(!localStorage.getItem('authId')){
        next({ path:'/login'});
      }else{
        next();
      }
  }else if(to.meta.AuthUserLR){
      if(localStorage.getItem('authId')){
        next({ path:'/'});
      }else{
        next();
      }
  }else{
      next();
  }
})

// Admin
router.beforeEach((to, from, next) => {
  if(to.meta.AdminAuth){
    if(!localStorage.getItem('AdminAuthId')){
      next({
          path:'/admin/login',
      });
    }else{
        next();
    }
  }else if(to.meta.AdminAuthLR){
    if(localStorage.getItem('AdminAuthId')){
      next({ path:'/admin'});
    }else{
      next();
    }
}else{
      next();
  }
})



export default router;
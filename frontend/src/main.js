

import { createApp } from "vue";
import App from "./App.vue";
import router from "@/router";
// import store from "./store";
import axios from 'axios';


axios.defaults.withCredentials = true;
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
axios.defaults.baseURL = process.env.VUE_APP_API_URL;



import userModule from "@/modules";
import adminModule from "@/modules/admin";

import { registerModules } from "@/register-modules";


registerModules({
    user: userModule,
    admin: adminModule,
});


const app = createApp(App);
app.use(router);
app.config.productionTip = false;
app.mount('#app');




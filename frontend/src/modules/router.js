const UserModule = () => import("@/modules/UserModule.vue");
const UserHomePage = () => import("@/modules/views/HomePage.vue");
// const UserTripSearchPage = () => import("@/modules/views/UserTripSearchPage.vue");
const UserLoginPage = () => import("@/modules/views/auth/LoginPage.vue");
const UserRegisterPage = () => import("@/modules/views/auth/RegisterPage.vue");
const UserForgetPassword = () => import("@/modules/views/auth/ForgetPassword.vue");
const UserResetPassword = () => import("@/modules/views/auth/ResetPassword.vue");
const UserVerifyEmail = () => import("@/modules/views/auth/VerifyEmail.vue");

const UserDashboardPage = () => import("@/modules/views/profile/DashboardPage.vue");
const UserTripBookedPage = () => import("@/modules/views/profile/UserTripBookedPage.vue");
const UserProfilePage = () => import("@/modules/views/profile/ProfilePage.vue");
const UserChangePassword = () => import("@/modules/views/profile/ChangePassword.vue");

const UserTripBookingPage = () => import("@/modules/views/TripBookingPage.vue");
const UserTripBookingSuccessPage = () => import("@/modules/views/TripBookingSuccessPage.vue");
const UserCarSellPage = () => import("@/modules/views/CarSellPage.vue");
const UserCarDetailsPage = () => import("@/modules/views/CarDetailsPage.vue");
const UserCarSearchPage = () => import("@/modules/views/CarSearchPage.vue");
const UserCarAdsPage = () => import("@/modules/views/CarAdsPage.vue");
const UserRentCarPage = () => import("@/modules/views/RentCarPage.vue");
const UserMyWalletPage = () => import("@/modules/views/MyWalletPage.vue");
const UserCodeTestPage = () => import("@/modules/views/CodeTestPage.vue");



const moduleRoute = {
    path: "/",
    component: UserModule,
    children: [
      { 
        path:"/", 
        name:"user-home",
        component:UserHomePage,
        meta: {AuthUser: false}
      },
      { 
        path:"/test", 
        name:"user-test",
        component:UserCodeTestPage,
        meta: {AuthUser: false}
      },
      { 
        path:"/login", 
        name:"user-login",
        component:UserLoginPage,
        meta: {AuthUserLR: true}
      },
      { 
        path:"/register", 
        name:"user-register",
        component:UserRegisterPage,
        meta: {AuthUserLR: true}
      },
      { 
        path:"/forget-password", 
        name:"user-forget-password",
        component:UserForgetPassword,
        meta: {AuthUserLR: true}
      },
      { 
        path:"/password-reset/:token", 
        name:"user-reset-password",
        component:UserResetPassword,
        meta: {AuthUserLR: true}
      },
      { 
        path:"/verify-email", 
        name:"user-verify-email",
        component:UserVerifyEmail,
        meta: {AuthUserLR: true}
      },


      { 
        path:"/dashboard", 
        name:"user-dashboard",
        component:UserDashboardPage,
        meta: {AuthUser: true}
      },
      { 
        path:"/user/trip-booked", 
        name:"user-trip-booked",
        component:UserTripBookedPage,
        meta: {AuthUser: true}
      },
      { 
        path:"/profile", 
        name:"user-profile",
        component:UserProfilePage,
        meta: {AuthUser: true}
      },
      { 
        path:"/change-password", 
        name:"user-change-password",
        component:UserChangePassword,
        meta: {AuthUser: true}
      },



      { 
        path:"/trip-booking/:id", 
        name:"user-trip-booking",
        component:UserTripBookingPage,
        meta: {AuthUser: true}
      },
      { 
        path:"/trip-booking/:id/success", 
        name:"user-trip-booking-success",
        component:UserTripBookingSuccessPage,
        meta: {AuthUser: true}
      },
      // { 
      //   path:"/trip-search?:location=&user=&type=", 
      //   name:"user-home",
      //   component:UserTripSearchPage,
      //   meta: {AuthUser: false}
      // },
      { 
        path:"/car-sell-exchange", 
        name:"user-car-sell",
        component:UserCarSellPage,
        meta: {AuthUser: false}
      },
      { 
        path:"/car-details", 
        name:"user-car-details",
        component:UserCarDetailsPage,
        meta: {AuthUser: false}
      },
      { 
        path:"/car-search", 
        name:"user-car-search",
        component:UserCarSearchPage,
        meta: {AuthUser: false}
      },
      { 
        path:"/car-ads", 
        name:"user-car-ads",
        component:UserCarAdsPage,
        meta: {AuthUser: false}
      },
      { 
        path:"/rent-a-car", 
        name:"user-rent-a-car",
        component:UserRentCarPage,
        meta: {AuthUser: false}
      },
      { 
        path:"/my-wallet", 
        name:"user-my-wallet",
        component:UserMyWalletPage,
        meta: {AuthUser: false}
      },
    ]
  };
  
  export default router => {
    router.addRoute(moduleRoute);
  };
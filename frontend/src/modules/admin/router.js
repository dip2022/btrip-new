const AdminModule = () => import("@/modules/admin/AdminModule.vue");
const AdminHomePage = () => import("@/modules/admin/views/HomePage.vue");
const AdminLogin = () => import("@/modules/admin/views/auth/AdminLogin.vue");
const AdminForgetPassword = () => import("@/modules/admin/views/auth/AdminForgetPassword.vue");
const AdminReturnTripPage = () => import("@/modules/admin/views/ReturnTripPage.vue");



const moduleRoute = {
    path: "/admin",
    component: AdminModule,
    children: [
      { 
        path:"/admin", 
        name:"admin-home",
        component:AdminHomePage,
        meta: {AdminAuth: true}
      },
      { 
        path:"/admin/login", 
        name:"admin-login",
        component:AdminLogin,
        meta: {AdminAuthLR: true}
      },
      { 
        path:"/admin/forget-password", 
        name:"admin-forget-password",
        component:AdminForgetPassword,
        meta: {AdminAuth: true}
      },
      { 
        path:"/admin/return-trips", 
        name:"admin-return-trips",
        component:AdminReturnTripPage,
        meta: {AdminAuth: true}
      },
    ]
  };
  
  export default router => {
    router.addRoute(moduleRoute);
  };